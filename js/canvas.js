'use strict';

const
    round = [],
    initRoundPopulation = 80,
    canvas = document.getElementById('canvas-container'),
    content = canvas.getContext('2d'),
    _canvasWidth = document.documentElement.clientWidth,
    _canvasHeight = document.documentElement.clientHeight;

canvas.width = _canvasWidth;
canvas.height = _canvasHeight;

/*****
 ** name: round_item
 ** date: 2017/12/06 11:50
 ** explain: x,y → 随机坐标 AND 随机透明度 AND 随机半径 ADN index   
 *****/
function round_item(x, y, index) {
    let
        _colorR = Math.floor(Math.random() * 256),
        _colorG = Math.floor(Math.random() * 256),
        _colorB = Math.floor(Math.random() * 256),
        _alpha = (Math.floor(Math.random() * 10) + 1) / 10 / 2;

    this._x = x;
    this._y = y;
    this._r = Math.random() * 2 + 1;
    this._index = index;
    this._color = 'rgba(' + _colorR + ', ' + _colorG + ', ' + _colorB + ', ' + _alpha + ')';
    /*****
     ** name: _draw
     ** date: 2017/12/06 12:05
     ** explain: canvas 画圆   
     *****/
    this._draw = function() {

        content.fillStyle = this._color; // 颜色
        content.shadowBlur = this._r * 2; // 阴影

        content.beginPath();

        content.arc(this._x, this._y, this._r, 0, 2 * Math.PI, false); // 圆
        content.fill();

        content.closePath();
    };
    /*****
     ** name: _move
     ** date: 2017/12/06 12:05
     ** explain: canvas y 轴数值变化   
     *****/
    this._move = function() {

        this._y -= 0.1;
        if (this._y <= -5) {
            this._y = _canvasHeight + 6;
        }

        this._draw();
    };
}

/*****
 ** name: animate
 ** date: 2017/12/06 15:00
 ** explain: canvas 擦出重绘 
 *****/
function animate() {

    content.clearRect(0, 0, _canvasWidth, _canvasHeight);

    for (let i in round) {

        round[i]._move();
    }

    requestAnimationFrame(animate);
}

/*****
 ** name: canvas_init
 ** date: 2017/12/06 12:15
 ** explain: 创建单个 round_item  ADN 初始化 
 *****/
function canvas_init() {

    for (let i = 0; i < initRoundPopulation; i++) {

        round[i] = new round_item(Math.random() * _canvasWidth, Math.random() * _canvasHeight, i);
        round[i]._draw();
    }

    animate();
}

canvas_init(); // start