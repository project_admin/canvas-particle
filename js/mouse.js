'use strict';

// 绘制全屏 canvas 元素

const
    canvas = document.getElementById('canvas-container'),
    conten = canvas.getContext('2d'),
    canvasWidth = canvas.width = document.documentElement.clientWidth,
    canvasHeight = canvas.height = document.documentElement.clientHeight;

let
    color_1,
    color_2,
    round_arr = [],
    // isRandom: flase 随机渐变色
    para = { num: 100, isRandom: false };

// 鼠标移动事件
window.addEventListener('mousemove', function(event) {
    const
        _mouseX = event.clientX,
        _mouseY = event.clientY;

    // r: 圆每次增加的半径
    // o: 判断圆消失的条件，数值越大消失越快
    round_arr.push = { r: para.r, o: 1, mouseX: _mouseX, mouseY: _mouseY };
}, false)

// 设置圆的颜色
if (para.isRandom) {
    color_2 = para.isRandom;
} else {
    color_1 = Math.random() * 360;
}

// 动画逻辑
let
    animate = function(_para, _color_1, _color_2) {

        this.para = _para;
        this.color_1 = _color_1;
        this.color_2 = _color_2;

        if (typeof this.init != 'function') {
            animate.prototype.init = function() {

                if (!this.para.isRandom) {
                    this.color_1 += .1;
                    this.color_2 = 'hsl(' + this.color_1 + ', 100%, 80%)';
                }

                conten.clearRect(0, 0, canvasWidth, canvasHeight);
                for (var i = 0; i < round_arr.length; i++) {
                    conten.filStyle = this.color_2;
                    conten.beginPath();
                    conten.arc(round_arr[i].mouseX, round_arr[i].mouseY, round_arr[i].r, 0, Math.PI * 2);
                    conten.closePath();
                    conten.fill();

                    round_arr[i].r += this.para.r;
                    round_arr[i].o -= this.para.o;

                    if (round_arr[i].o <= 0) {
                        round_arr.splice(i, 1);
                        i--;
                    }
                }
                console.log(this.color_2);
                window.requestAnimationFrame(this.init);
            }
        }
    },
    _animate = new animate(para, color_1, color_2);

_animate.init();