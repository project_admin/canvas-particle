'use strict';

(function() {

    let
        canvas = document.getElementById('canvas-container'),
        contxt = canvas.getContext('2d'),
        _width = canvas.width = 400,
        _height = canvas.height = 400,
        _gra = contxt.createLinearGradient(0, 0, 0, 400);

    contxt.beginPath();

    // 绘制弧 AND 圆
    contxt.arc(100, 100, 50, 0, Math.PI * 2, false);

    // 填充
    contxt.fillStyle = 'rgba(255, 255, 255, .6)';

    // 阴影
    contxt.shadowBlur = 20;
    contxt.shadowColor = 'rgba(255, 255, 255, 1)';

    contxt.fill();

    // 直线
    // contxt.moveTo(10, 10);
    // contxt.lineTo(200, 200);
    // contxt.lineWidth = 10;
    // contxt.lineCap = 'round';

    // 描边
    // contxt.strokeStyle = 'rgba(255, 255, 255, 1)';
    // contxt.stroke();

    // 矩形 -- 实心
    // contxt.fillStyle = 'rgba(255, 255, 255, .6)';
    // contxt.fillRect(10, 10, 100, 100);

    // 矩形 -- 空心
    // contxt.strokeStyle = 'rgba(255, 255, 255, 1)';
    // contxt.strokeRect(10, 10, 100, 100);

    // 渐变色
    // _gra.addColorStop(0,'rgb(255, 0, 0)');
    // _gra.addColorStop(0.2,'rgb(255, 165, 0)');
    // _gra.addColorStop(0.3,'rgb(255, 255, 0)');
    // _gra.addColorStop(0.5,'rgb(0, 255, 0)');
    // _gra.addColorStop(0.7,'rgb(0, 127, 255)');
    // _gra.addColorStop(0.9,'rgb(0, 0, 255)');
    // _gra.addColorStop(1,'rgb(139, 0, 255)');
    // contxt.fillStyle = _gra;
    // contxt.fillRect(0, 0, 400, 400);

    // 缩放
    // contxt.strokeStyle = 'rgba(64, 158, 255, 1)';
    // contxt.strokeRect(10, 10, 100, 50);
    // contxt.scale(1.5, 1.5);
    // contxt.strokeRect(10, 10, 100, 50);
    // contxt.scale(1.5, 1.5);
    // contxt.strokeRect(10, 10, 100, 50);

    // 旋转
    // contxt.fillStyle = 'rgba(64, 158, 255, 1)';
    // contxt.rotate(10 * Math.PI / 180);
    // contxt.fillRect(50, 50, 120, 120);

    contxt.closePath();
})()