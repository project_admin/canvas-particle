'use strice';

const
    canvas = document.getElementById('canvas-container'),
    content = canvas.getContext('2d'),
    _cWidth = canvas.width = document.documentElement.clientWidth,
    _cHeight = canvas.height = document.documentElement.clientHeight,
    _centreX = _cWidth / 2,
    _centreY = _cHeight / 2;


function random_Num(x, y) {

    return Math.floor(Math.random() * (y - x + 1) + x);
}

function random_Color(color) {

    let
        _r = random_Num(0, 256),
        _g = random_Num(0, 256),
        _b = random_Num(0, 256),
        _a = (Math.floor(Math.random() * 10) + 1) / 10 / 2,
        _rgb = _r + ',' + _g + ',' + _b;
    if (color === 'rgb') {

        return 'rgb(' + _rgb + ')';
    } else {

        return 'rgba(' + _rgb + ',' + _a + ')';
    }
}

function canvas_Logic() {

    // 圆半径 ADN 颜色
    this.r = random_Num(.1, 2);
    this.color = random_Color('rgb');

    // 随机坐标
    this.x = random_Num(this.r, _cWidth - this.r);
    this.y = random_Num(this.r, _cHeight - this.r);

    // 移动距离
    this.speedX = random_Num(1, 3) * (random_Num(0, 1) ? 1 : -1);
    this.speedY = random_Num(1, 3) * (random_Num(0, 1) ? 1 : -1);
}

canvas_Logic.prototype.move = function() {

    this.x = this.speedX * .2;
    this.y = this.speedY * .2;

    if (this.x <= this.r) {

        this.x = this.r;
        this.speedX *= -1;
    }

    if (this.x >= _cWidth - this.r) {

        this.x = _cWidth - this.r;
        this.speedX *= -1;
    }

    // 圆点碰到边界处理
    if (this.y <= this.r) {

        this.y = this.r;
        this.speedY *= -1;
    }

    if (this.y >= _cHeight - this.r) {
        this.y = _cHeight - this.r;
        //反弹
        this.speedY *= -1;
    }
}

canvas_Logic.prototype.draw = function() {

    content.beginPath();

    content.arc(this.x, this.y, this.r, 0, Math.PI * 2, false);
    content.fillStyle = this.color;

    content.fill();
}

let
    arr = [],
    balls = [];

// 存储圆点到 balls 数组
for (let i = 0; i < .0002 * _cWidth * _cHeight; i++) {

    let
        _cLogic = new canvas_Logic();

    balls.push(_cLogic);
}


function ballAndBall(obj1, obj2) {

    let
        disX = Math.abs(obj1.x - obj2.x),
        disY = Math.abs(obj1.y - obj2.y);

    return Math.sqrt(disX * disX + disY * disY);
}

function ballAndMouse(obj) {

    let
        disX = Math.abs(_centreX - obj.x),
        disY = Math.abs(_centreY - obj.y);

    return Math.sqrt(disX * disX + disY * disY);
}

setInterval(function() {

    // 初始化
    arr = [];
    content.clearRect(0, 0, _cWidth, _cHeight);

    for (let i = 0; i < balls.length; i++) {
        balls[i].move();
        balls[i].draw();
        if (ballAndMouse(balls[i]) < 230) {

            content.lineWidth = (230 - ballAndMouse(balls[i])) * 1.5 / 230;

            content.beginPath();

            content.moveTo(balls[i].x, balls[i].y);
            content.lineTo(_centreX, _centreY);
            content.strokeStyle = balls[i].color;

            content.stroke();
        }
    }

    for (let i = 0; i < balls.length; i++) {
        for (let j = 0; j < balls.length; j++) {
            if (ballAndBall(balls[i], balls[j]) < 230) {

                content.lineWidth = (230 - ballAndBall(balls[i], balls[j])) * 0.6 / 230;
                content.globalAlpha = (230 - ballAndBall(balls[i], balls[j])) * 1 / 230;

                content.beginPath();

                content.moveTo(balls[i].x, balls[i].y);
                content.lineTo(balls[j].x, balls[j].y);
                content.strokeStyle = balls[i].color;

                content.stroke();
            }
        }
    }
    content.globalAlpha = 1.0;
}, 30);