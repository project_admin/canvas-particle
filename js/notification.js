'use strict';

const
    notificationInstance = Notification || window.Notification;

if (!!notificationInstance) {
    const
        isPermission = notificationInstance.permission;

    let
        setPermission = function() {
            // 请求通知权限
            notificationInstance.requestPermission(function(PERMISSION) {
                if (PERMISSION === 'granted') {
                    creatNotification();
                } else {
                    console.log('用户拒绝通知.');
                }
            })
        },
        creatNotification = function() {
            let
                n = new notificationInstance('客服系统--通知', {
                    body: '客户发来新消息,请查看后尽快回复!',
                    tag: 'Info',
                    icon: './header.jpg',
                    renotify: true,
                    data: {
                        url: 'https://2ue.github.io'
                    }
                });

            n.onerror = function(err) {
                throw err;
                console.log('出错了，小伙子在检查一下吧');
            }

            // setTimeout(() => {
            //     n.onclose = function() {
            //         console.log('客户消息.');
            //     }
            // }, 500);
        };

    if (isPermission === 'granted') {
        // 允许通知
        creatNotification();
    } else if (isPermission === 'denied') {
        console.log('用户拒绝通知.')
    } else {
        // 请求权限
        setPermission();
    }
}